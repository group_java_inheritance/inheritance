/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance;

/**
 *
 * @author BOAT
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani","White",0);
        animal.speak();
        animal.walk();
        
        Dog dang= new Dog("Dang", "Black&White");
        dang.speak();
        dang.walk();
        
        Dog To= new Dog("To", "oreange&White");
        To.speak();
        To.walk();
        
        Dog Mome = new Dog("Mome", "Black&White");
        Mome.speak();
        Mome.walk();
        
        Dog Bat= new Dog("Bat", "Black&White");
        Bat.speak();
        Bat.walk();
        
        Cat zero = new Cat("zero", "oreange");
        zero.speak();
        zero.walk();
        
        Duck zom = new Duck("zom", "oreange");
        zom.speak();
        zom.walk();
        zom.fly();
        
        Duck GabGab = new Duck("GabGab", "oreange");
        GabGab.speak();
        GabGab.walk();
        GabGab.fly();
        
        System.out.println("Zom is Animal: "+(zom instanceof Animal));
        System.out.println("Zom is Duck: "+(zom instanceof Duck));
        System.out.println("Zom is Cat: "+(zom instanceof Object));
        System.out.println("Animal is Dog: "+(animal instanceof Dog));
        System.out.println("Animal is Animal: "+(animal instanceof Animal));
        
        System.out.println("----------------------------");
        
        System.out.println("GabGab is Animal: "+(GabGab instanceof Animal));
        System.out.println("GabGab is Duck: "+(GabGab instanceof Duck));
        System.out.println("GabGab is Cat: "+(GabGab instanceof Object));
        System.out.println("Animal is Cat: "+(animal instanceof Cat));
        
        System.out.println("----------------------------");
        
        System.out.println("zero is Animal: "+(zero instanceof Animal));
        System.out.println("zero is Cat: "+(zero instanceof Cat));
        System.out.println("zero is Dog: "+(zero instanceof Object));
        System.out.println("Animal is Duck: "+(animal instanceof Duck));

        System.out.println("----------------------------");
        
        System.out.println("Bat is Animal: "+(Bat instanceof Animal));
        System.out.println("Bat is Dog: "+(Bat instanceof Dog));
        System.out.println("Bat is Cat: "+(Bat instanceof Object));
        System.out.println("Animal is Dog: "+(animal instanceof Dog));
        
        System.out.println("----------------------------");
        
        System.out.println("Mome is Animal: "+(Mome instanceof Animal));
        System.out.println("Mome is Dog: "+(Mome instanceof Dog));
        System.out.println("Mome is Cat: "+(Mome instanceof Object));
        System.out.println("Animal is Dog: "+(animal instanceof Dog));
        
        System.out.println("----------------------------");
        
        System.out.println("To is Animal: "+(To instanceof Animal));
        System.out.println("To is Dog: "+(To instanceof Dog));
        System.out.println("To is Cat: "+(To instanceof Object));
        System.out.println("Animal is Dog: "+(animal instanceof Dog));
        
        System.out.println("----------------------------");
        
        System.out.println("dang is Animal: "+(dang instanceof Animal));
        System.out.println("dang is Dog: "+(dang instanceof Dog));
        System.out.println("dang is Cat: "+(dang instanceof Object));
        System.out.println("Animal is Dog: "+(animal instanceof Dog));
        System.out.println("----------------------------");
        
        Animal ani1 = null;
        Animal ani2 = null;
        ani1 = zom;
        ani2 = zero;
        
        System.out.println("Ani1:  zom is Duck "+(ani1 instanceof Duck));
        
        Animal[] animals = {dang, zero,zom,GabGab,To,Mome,Bat};
        for(int i=0; i< animals.length; i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i]; 
                duck.fly();
            }
        }
}
}